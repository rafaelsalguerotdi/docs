# Comunicación
- Encargado de desarrollo será responsable de contestar a tiempo los siguientes canales de comunicación.
    - Correos o llamadas provenientes de los coordinadores de cambios o nuevos requerimientos.
    - Correos de errores provenientes de coordinadores o ingenieros.
    - Los correos y llamadas deben de estar dirigidos al encargado de desarrollo, no a los programadores.

# Pendientes

## Dudas
- Encargado de desarrollo recibirá dudas sobre los sistemas enviadas por los coordinadores por correo o llamada.
- Estas dudas podrán ser delegadas a los programadores por el encargado de desarrollo.
- Los programadores no recibirán directamente las dudas de ingenieria.

## Cambios o nuevos desarrollos
- Coordinador de servicio será responsable de:
    - Enviar por correo el requerimiento del pendiente, lo más detallado posible.
    - Sólo se aceptarán correos de pendientes nuevos enviados por el coordinador de servicio.

- Encargado de desarrollo será responsable de: 
    - Dar un folio de ticket para el seguimiento del pendiente.
    - De solicitar toda la información necesaria para el pendiente.
    - Si ya se recabo toda la información, dar una fecha de entrega en caso de ser requerida.
    - Autorizar la comunicación entre los programadores y los ingenieros si es necesaria para el desarrollo del pendiente.
    - Delegar el seguimiento del ticket a los programadores.

- Programadores responsables reportar al encargado de desarrollo:
    - Sus estimaciones de tiempos.
    - En caso de no poder dar el estimado, explicar la necesidad de más información.
    - Avisar en caso de necesitar más información o comunicación con los ingenieros.

- Programadores al finalizar un pendiente:
    - Mandarán un correo incluyendo el folio de ticket indicando que ya se terminó el ticket, pidiendo confirmación ya sea al ingeniero o al coordinador que revise la finalización del pendiente.
    - En caso de que se confirme el desarrollo terminado, o que se tenga 1 día entero sin respuesta del correo, se considerará el pendiente terminado, se cerrará el ticket y no se le dará más seguimiento.

## Reporte de errores
- A diferencia de los cambios o nuevos desarrollos, si se aceptarán correos con reportes de errores enviados por los ingenieros
- Desarrollo será responsable de:
    - Dar un folio de ticket para el seguimiento del error
    - Requerir la información necesaria para arreglar el problema