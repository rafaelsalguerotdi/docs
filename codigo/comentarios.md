# Comentarios
El objetivo es proporcionar suficiente información para que cualquier programador pueda entender el funcionamiento código
sin necesidad de conocer a profunidad las ideas originales o de leerlo en su totalidad.

Los siguientes comentarios son obligatorios para cualquier código nuevo:

## Orden de la documentación
La documentación se debe de escribir **antes** de escribir el código.

## Información en la documentación
La documentación no debe de ser sólo una copia del nombre de lo que se esta documentando, se debe de proporcionar información útil en la misma.

## Nomenclatura
- Nombrar de forma descriptiva las clases y todos sus miembros
- Sólo usar nombres cortos en variables cuando estas tengan un *scope* pequeño

## Métodos
- El objetivo debe de ser que un programador pueda entender el funcionamiento del método leyendo sólo su firma, sin necesidad de
 leer el cuerpo del mismo.
- Aplica para todos los métodos, incluso para los métodos privados.
- Describir su propósito, en caso de que un método tenga varios propósitos, considerar dividirlo en varios métodos más pequeños.
    - Note que el propósito del método va más allá de que operaciones hace, ya que aqui se pueden tocar temas de la lógica de negocio que no están
    expresados en el código.
- Describir el retorno.
- Describir los parámetros ya sea de forma resumida en el resumen de los comentarios, o de forma individual cada parámetro en caso de que sea más complicada su función en el método.
- Describir los rangos de los parámetros. (Acepta nulos, no acepta cadenas vacias, etc...)
- Describir las condiciones excepcionales (cuando lanza excepciones,  cuando devuelve null)
- En caso de que el método tenga varias divisiones lógicas en su cuerpo, describir estos pasos
- Al tener multiples opciones para implementar el método, justificar el camino que se tomó con los comentarios.

## Clases
- El objetivo es expresar el alcance de la clase, de tal manera que un programador pueda entender para que se usa esa clase, y 
que código puede ir en la misma en caso de que se tenga que extender.
    - Es importante respetar la definición de la clase y no agregar funcionalidad que no le corresponda a la misma para no terminar con un *God Class*

### Propiedades y campos
- Se debe de documentar cuando se cumpla cualquiera de estas condiciones:

    - Tenga una interacción con la lógica de negocios
    - Si es accesado por cualquier otra propiedad o método de la clase.
    - Su función sea más complicada que ser un simple campo en una ventana de captura.
    - Su nombre no describa directamente el significado del misma

### Miembros con `override` o implementaciones de interfaz
- Se puede considerar no documentar los miembros (métodos y propiedades) `override` o que sean una implementación de una interfaz en caso de que su función este descrita por completo en la clase padre o interfaz.

### Constructores
- Documentar cuando:
    - No sea una simple asignación directa de las propiedades de la clase

# Documentación de código existente
El objetivo es ir mejorando la documentación paulatinamente, comenzando con las partes del código que sufren más modificaciones.

Cada que se trabaje sobre una parte del código, documentar ese parte, la clase a la que pertenece y las clases relacionadas al mismo (puede ser relacionada por el tipo de retorno, por los parámetros o por los tipos de las variables del cuerpo del mismo)