# git

## Control de código distribuído
Todas las operaciones en git se hacen de forma local, sólo se comparten con el servidor al hacer push, pull o fetch

## Conceptos
- `commit` - Es un *snapshot* de todo el proyecto en cierto punto de la historia, los commits son inmutables e indelebles. Todos los commits tienen *ancestros* que son ya sea el commit anterior, o en caso de ser un `merge`, los commits de los que proviene.
- `branch` - Un puntero a un commit, al hacer commit sobre la rama actual estos serán cambios independientes de las demás ramas.
- `HEAD` - Es el puntero que apunta al último commit de la rama actual.
- `working tree` - Son los archivos físicos que estan manejados por el git.
- `index`/`staging area` - Una lista con los archivos marcados para ser agregados al siguiente commit.
- `master` - Es el nombre que se le da por default a la rama principal. Se trata de que esta rama siempre este compilando, dejando los trabajos en progresos en otras ramas.
- `remote` - El servidor de git

## `merge`
 Una operación que crea un commit especial el cual proviene de 2 commits, un merge es necesario cuando se quieren mezclar dos ramas que tienen commits posteriores a su ancestro comun mas cercano. 

Note que un merge al ser también un commit, es completamente reversible y no es posible perder el trabajo realizado en los commits anteriores.

Ejemplo:

```
a---b---c---d---f   <- master
         \--e---g   <- prueba
```

El ancestro común entre las ramas `master` y `prueba` es el commit `c`. Para pasar el progreso de `prueba` a `master` se tendrá que hacer un `merge`, ya que en ambas ramas existen commits posteriores, que son `d, f` para `master` y `e, g` para prueba.

Después del merge las ramas quedarán:
```
a---b---c----d---f--\ 
         \           \----h <- master
          \--e---g --/      <- prueba
```

El commit `h` es un commit de `merge`, el cual tiene de ancestros a los commits `f, g`.

Después de un merge la rama actual apuntará al commit creado por el merge, la rama de origen no será modificada.

## `git status`
Muestra la rama actual, si hay cambios pendientes y si la rama actual esta sincronizada con el `remote`.

Note que este comando no hace peticiones al servidor, es completamente local. La verificación contra el remote se hace contra la copia local del remote.

## Ramas
A diferencias de otros SCCS, las ramas en git son *lightweight*, se crean en segundos y forman parte del flujo diario de la operación.

Algunas aplicaciones de las ramas:

- Crear una rama para probar una idea experimental, en cualquier momento se podrá volver a la rama anterior o mezclar esta rama.
- Tener una rama para el código que está en producción, de esta forma se pueden resolver problemas en producción rápidamente sin tener que terminar el trabajo en progreso en las otras ramas.
- Crear una rama para cierta característica, de esta forma se podrán aislar los cambios y mezclar cuando sea necesario.
- Crear una rama para trabajar sobre una versión anterior del proyecto, incluso una que nunca tuvo una rama propia.

### Crear una rama:
En Visual Studio, click derecho en la rama y `Create branch from...` o correr `git checkout -b nuevarama`

### Cambiar la rama actual
En Visual Studio, doble click en la rama o correr `git checkout otrarama`

## push, pull, fetch y sync
- La operación más comun es el `sync`, lo cual baja los cambios del `remote` (`git pull`), e intenta subir los cambios (`git push`). Es posible que la operación `push` falle en caso de que existan conflictos con los cambios, en ese caso se tendrá que hacer un `merge`.

La operación `fetch` actualiza los datos locales del remote, normalmente esta operación se hace de forma indirecta al hacer `sync`, `pull` o `push`.


## Ver diferencias
En Visual Studio se puede ver usando el Team Explorer:

- La historia de un archivo
- Quien y en que commit se hizo cada linea de un archivo (`git blame`)
- Diferencias entre commits o ramas

## Deshacer cambios
Lo mejor es hacer un commit, incluso si el trabajo que se hizo esta mal o incompleto, y después hacer un 
`revert`, esto dejará el código en su estado original pero se conservará la historia de lo que se hizo. De esta forma se podrá recuperar cualquier código anterior.

Otra opción es deshacer todos los cambios pendientes desde el último commit con `git reset --hard` o en el Visual seleccionar todos los cambios pendientes y darle `Undo`, note que esta operación no se puede deshacer y se pierde todo el progreso desde el último commit.

## Limpiar todo
Para borrar todos los archivos y carpetas que no están incluídos en el git correr `git clean -xd -f`
Todos los proyectos deberían de poder compilar después de ejecutar este paso, si no es así, significa que el proyecto tiene dependencias que no estan en el git.

## *Untrack* archivos basandose en el `.gitignore`
Puede ser que existan archivos incluidos en el git pero que se quieren ignorar.
Para ignorar todos los archivos que entren en los filtros de `.gitignore`, incluso si ya estan agregados al repositorio:

1.- Borrar todo del repositorio (sólo del `index`)
```
git rm -r --cached .
```

- `rm` Comando para borrar archivos
- `-r` Borrado recursivo
- `--cached` Sólo borrar del `index`, no borrar los archivos físicamente
- `.` Es un `pathspec`, indica que todos los archivos entran al filtro

2.- Agregar todo, esto agregará solo los archivos que no son ignorados por el `.gitignore`
```
git add .
```
3.- Hacer el commit que tendrá operaciones de borrado para todos los archivos ignorados.
``` 
git commit -m "Borrar archivos"
```