# Catálogos
## Agregar una unidad
Los clientes los manejaremos como "Unidades"
Para agregar una unidad:
- Ir a `Catálogos > Unidades > Agregar`
- Capturar la zona, nombre, nombre corto, estado, ciudad e institución
- Guardar

**Nota:**
Para poder utilizar la unidad en los tickets hay que asignarla al contrato general y a los sistemas de informática que corresponde.

### Asignar la unidad al contrato `General`
- Ir a `Catálogos > Clientes maestros > Editar cliente "General" `
- Ir a `Contratos > Editar contrato "General"`
- Seleccionar la unidad que falta en la lista de unidades del contrato
- Guardar

### Asignar la unidad a un sistema de informática
- Ir a `Catálogos > Sistemas de informática > Editar el sistema de interés`
- En `Unidades y servicios` click en agregar
- Seleccionar la unidad en la que esta instalado el sistema y en servicio, seleccionar "General"
- Guardar y guardar