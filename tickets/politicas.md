# Sistema de tickets
## Objetivos
- Evitar problemas con los clientes formalizando sus requerimientos, dando a los mismos un folio y correo para su seguimiento
- Justificar nuestro trabajo y cobranza a los clientes, dando un listado detallado de todo lo trabajado en cierto periodo de facturaciÃ³n
- Controlar los pendientes de desarrollo y publicaciones de tal manera que nunca se dejen errores o desarrollos sin atención
- Formar una base de datos de bugs

## Pasos
### Cuando crear un ticket
- Al recibir un requerimiento ya sea por correo o llamada
- Normalmente los desarrolladores no recibirán requerimientos directamente pero en caso de hacerlo, hay que levantar el ticket.
- Al interactuar con el cliente ya sea interno o externo, siempre se debe de registrar esta información ya sea en un ticket o nota de ticket.
- Los ticket no solamente son requerimientos de desarrollo, cualquier trabajo realizado por desarrollo debe de tener un ticket.
- Sólo deben de existir ticket para trabajo que se deba realizar en desarrollo, no crear tickets para pendientes de otras áreas.

### Responsabilidad de la persona que recibe el requerimiento / encuentre un bug
- Crear el ticket
- Para tickets que provengan de un requerimiento de un cliente: Enviar un correo al cliente con el folio del ticket
- Asignar el ticket al desarrollador que corresponde

### Responsabilidad del desarrollador
- No realizar ningún desarrollo sin tun ticket, todo trabajo realizado debe de estar respaldado por un ticket sin excepción
- Detallar la solución del ticket en las notas del mismo. Ej. origen del bug, explicaciónes, etc...
- Detallar las interacciones con los  clientes en las notas, cualquier detalle o duda.
- Si hay pendientes nuevos relacionados muy pequeños que no implican otro ticket agregarlo al problema reportado del ticket, si no, agregar otro ticket
- Al terminar el ticket cerrarlo y avisarle al cliente y al encargado con correo con el folio de ticket cerrado.
- Si se termino el trabajo de nuestro lado, pero sabemos que se tendrá problemas con la implementación del lado de los ingenieros,
si cerrar el ticket pero detallar esto en las notas del mismo.