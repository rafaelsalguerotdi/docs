# Agregar un ticket
- Ir a `Listado > + Informática` o en `Inicio > Nuevo ticket de informática`
- Los datos obligatorios y que no son por default son
    - Sistema de informática
    - Unidad
    - Problema reportado
    - Contacto reportó (sólo el nombre basta)
    - Una nota indicando la referencia del ticket, ya sea una parte del asunto o cuerpo del correo o la fecha de la llamada
- Datos obligatorios pero con valores generales:
   
   | Campo                 | Valor     |
    |-----------------------|-----------|
    | Área                  | General   |
    | Contrato              | General   |
    | Requirió refacción    | No        |
    | Orden de servicio     | No        |
    | Servicio              | General   |
    | Institución           | General   |
    
- Click en `Guardar y asignar`
- Asignar al ingeniero que le corresponde ese ticket